# demo-cap-pg-os4

Demo running CAP with PostgreSQL on Open Shift 4

Sample CAP app is based on the example described [here](https://blogs.sap.com/2020/11/16/getting-started-with-cap-on-postgresql-node.js/).

# Dockerfile

To be able to run CAP with PostgreSQL in Open Shift 4, you need a container that includes Node.js and Java. To be able to deploy the CSV files and to do the delta handling of the database in case of updates, CDS-DBM uses a 3rd party software: Liquidbase. That software is written in Java and needs a Java JRE. As Open Shift uses S2I and the standard images provided do either contain Java or Node.js, but not both, you can

a) create a new custom S2I image containing Java and Node.js, or

b) you let Open Shift build a custom Docker image.

The demo porject creates a custom Docker image. For this, the Dockerfile is included. To use it, make sure to build the project to Docker.

# Open Shift 4

If you have an Open Shift 4 cluster available, you can start right away. For everybody else, if you want to have Open Shift 4 on your laptop or home lab running, install [RedHat CodeReady Containers](https://developers.redhat.com/products/codeready-containers/overview). You need a Red Hat developer account (free). Follow the (steps from the installation site](https://cloud.redhat.com/openshift/create/local).

## Create project

Create a new project in a custom namespace, like cap.

## PostgreSQL database

Add a new PostgreSQL database to your project. From the library, select PostgreSQL version 12 with persistent storage. During the creation, a set of secrets will be created. If you want, you can use your own credentials. These values are needed by CAP later to be able to connect to the database!

# Secrets

For the CAP container, you have to create a set of credentials. These will be provided to the running pod as environmental variables.


[CAP documentation](https://cap.cloud.sap/docs/node.js/cds-connect#cds-env-requires))

While [xsenv](https://www.npmjs.com/package/@sap/xsenv) can use secret mount points to find out the credentials and pass them to CAP, for some reasons this is not working as CAP is not able to resolve these values to valid credentials for the database service. What [CAP expects](https://cap.cloud.sap/docs/node.js/cds-env#process-env) and what xsenv delivers does not match. The lookup for the credentials for the database service with kind/name is always returning an empty object.

The file openshift/secrets.yaml contains the credentials for CAP to connect to PostgreSQL. The values will be provided to the pod via environment variables.

```yaml
data:
  database: c2FtcGxlZGI=
  host: cG9zdGdyZXNxbA==
  password: ZGV2ZWxvcGVy
  port: NTQzMg==
  schema: cHVibGlj
  user: ZGV2ZWxvcGVy
```

## CAP

DeploymentConfig for container

### Secrets

Provide values for secrets via environmental variables that are read from the secret.

```yaml
spec:
    volumes:
        - name: capire
          secret:
            secretName: capire
            defaultMode: 420        
    containers:
        env:
            - name: CDS_REQUIRES_DB_CREDENTIALS_DATABASE
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: database
            - name: CDS_REQUIRES_DB_CREDENTIALS_HOST
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: host
            - name: CDS_REQUIRES_DB_CREDENTIALS_USER
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: user
            - name: CDS_REQUIRES_DB_CREDENTIALS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: password
            - name: CDS_REQUIRES_DB_CREDENTIALS_PORT
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: port
            - name: CDS_REQUIRES_DB_CREDENTIALS_SCHEMA
              valueFrom:
                secretKeyRef:
                  name: capire
                  key: schema
          resources: {}
```

### Volume store

To load the tables and changes, cds-dbm uses liquibase and needs to store temporary local data for the delta. In OpenShit 4, the storage is read only. You need to provide a writeable temporary storage. 




psql -d sampledb
\dt

